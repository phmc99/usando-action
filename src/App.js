import "./App.css";

import { useDispatch, useSelector } from "react-redux";
import { changeName } from "./store/modules/user/actions";
import { useState } from "react";

function App() {
  const user = useSelector((state) => state.user);
  const [newName, setNewName] = useState("");
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(changeName(newName));
  };

  return (
    <div className="App">
      <div>
        <span>{user.name}</span>
        <input
          type="text"
          value={newName}
          onChange={(event) => setNewName(event.target.value)}
        />
        <button onClick={handleClick}>Change</button>
      </div>
    </div>
  );
}

export default App;
